O objetivo deste trabalho é utilizar o algoritmo de compactação de mensagens de Huffman para
criar arquivos binários (ou texto) compactados.
Descrição

O algoritmo de Huffman sugere um esquema de codificação para o alfabeto de uma mensagem
em função da frequência de cada símbolo. Essa codificação é representada através de uma
árvore binária. Cabe a você pesquisar a literatura e entender a forma de criar essa árvore e
codificar a mensagem, apesar de você pode inserir em seu trabalho prático implementações já
existentes na literatura para o algoritmo de Huffman.
=======================================================================================

=======================================================================================

O que deve ser implementado

Uma vez entendido o algoritmo de Huffman, a mensagem (arquivo binário/texto) deverá ser
codificada e armazenada em arquivo. No entanto, antes é recomendado que a árvore que gera
a codificação seja armazenada no cabeçalho do arquivo, para posterior descompactação.
Lembre-se que o tamanho da árvore é variável - ele depende do tamanho do alfabeto. Cabe a
você desenvolver o TAD para esta árvore e a forma de armazenamento. Ressalta-se que essa é
a parte principal do trabalho, e cópias serão severamente punidas, não somente zerando o valor
deste trabalho, mas também eliminando a maior nota dos testes teóricos. Você também deverá
implementar uma função para descompactar o arquivo. Com essa segunda função
implementada, você poderá facilmente testar se o seu trabalho prático está funcionando
perfeitamente.
O seu programa deverá funcionar em linha de comando. Caso você implemente uma interface
gráfica, você poderá ganhar até 10 pontos extras, dependendo da qualidade da interface.
Deseja-se também que o tamanho dos arquivos compactados pelo algoritmo de Huffman sejam
comparados com os gerados por compactadores bem conhecimentos como ZIP e RAR, TAR.
Uma rápida análise dessa comparação deve ser feita.

Tipos e Tamanhos de Arquivos a serem utilizados

Procure utilizar arquivos texto, imagens (em diferentes formatos, com e sem compactação),
vídeos, áudios, de sistema (.dll, .exe, etc), etc. Escolha arquivos de diversos tamanhos variando
desde 1 Kilobytes até 100 Megabytes.
=======================================================================================

Implementação do algoritmo de Huffman

Para este trabalho, está sendo indicado o uso da implementação do algoritmo de Huffman
implementado em [1,2]. Todavia, outros códigos podem utilizados. Eventuais adaptações e
correções no código são objetivo de avaliação deste trabalho.
[1] N. Ziviani, F.C. Botelho, Projeto de Algoritmos com implementações em Java e C++, Editora
Thomson, 2006.
[2] Aaron M. Tenenbaum, Yedidyah Langsam, Moshe J. Augenstein, Estruturas de Dados Usando
C, Makron Books/Pearson Education, 1995.
=======================================================================================
O que deve ser entregue:
 Projeto.
 Documentação:
> Descrição sobre a implementação do programa:
Deve ser detalhada a estrutura de dados utilizada (de preferência com diagramas ilustrativos), o
funcionamento das principais funções e procedimentos utilizados, o formato de entrada e saída de
dados, bem como decisões tomadas relativas aos casos e detalhes de especificação que porventura
estejam omissos no enunciado.
Os códigos utilizados na implementação devem ser inseridos na documentação.
=======================================================================================
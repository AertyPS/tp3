import java.io.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;//importes de leitura e escrita de arquivos

public class Arvore {

     
     private NoA raiz;//raiz da arvore
     private static int R = 256; // alfabeto ASCII
     private static String A; // nova tree
     private static int T = 0;//tamanho original do texto que vem no arquivo comprimido
     private static int TA = 0;////tamanho original da arvore que vem do arquivo comprimido
    // private Descritor listaTabela;

     public Arvore(){
         raiz = null;
     }
 
     public boolean isEmpty(NoA r){
         return r == null;
     }

    public NoA montar(int frequencia, char caracter,NoA direito, NoA esquerdo){//na verdade faz a junção

        NoA noA = new NoA(caracter, frequencia, esquerdo, direito);
        raiz = noA;

        return noA; // Retorna o endereço da 1ª raiz.

    }

       
    public String[] ConstruirTabela() { //chama a funcao recussiva que ira percorrer a arvore
            System.out.print("\n-------Tabela------\n");
            String[] st = new String[R];
            Tabela(this.raiz, st, "");
            return st;
    }

    private void Tabela(NoA r, String[] st, String cd){//faz recursividade para montar a tabela

            if(!isEmpty(r)){
                Tabela(r.getEsquerdo(), st, cd +'0');//se vai para esquerda adiciono 0
                Tabela(r.getDireito(), st, cd +'1');//se vai para direita adiciono 1

                if(r.getObjeto().getCaracter() != '\0'){//no folha
                    System.out.print("\nCaracter: " + r.getObjeto().getCaracter()+" "+cd);//imprimi a tabela

                    try {
                        st[r.getObjeto().getCaracter()] = cd;//inserçao do codigo no vetor de strings

                    } catch (ArrayIndexOutOfBoundsException e) {
                        System.out.print("\n\n\nO texto de entrada possui o caracter: "+r.getObjeto().getCaracter());
                        System.out.print(" \nque não esta presente na tabela ASCII");
                        System.out.print("\nDica: troque o caracter por um similar presente na tabela ASCII \n\n");
                        System.exit(0);
                    }
                   
                }
                
            }
            
    }
    
    private String escreverArvore(NoA r, String s){//comprime a arvore em preordem adiciono 0 nos nós ao passar pelos nao folhas 
        String texto = s;                           //adiciono 1 ao chegar em um nó folha e em seguida adiciono o  
            if(!isEmpty(r)){                        //caracter em binario.
            
                if(r.getObjeto().getCaracter() != '\0'){//nó folha nao tem o caracter \0
                    texto += '1';
                    int i = (int) r.getObjeto().getCaracter();//converte char para ascII
                    String binstr = converteDecimalParaBinario(i);//converte decimal para binario(o binario tem que ter exatamente 8 bits)
                    texto = texto + binstr;//inserçao do codigo no vetor de strings
                    //System.out.print("\nCaracter: " + r.getObjeto().getCaracter()+" "+texto);//para saber se esta ok
                    return texto;
                }

                texto += '0';//nós intermediarios
                texto = escreverArvore(r.getEsquerdo(),texto);
                texto = escreverArvore(r.getDireito(),texto);
                
            }
            return texto;
    }

    private static NoA leituraArvore(String[] st) {//remonta a arvove pelo arquivo comprimido

            NoA r = null;
            
            if (st[0].equals("1")) {//se for 1 é o no folha transformo p decimal e depois p caracter ascII

                reduzString(st,1);
                char c = (char)(converteBinarioParaDecimal(A.substring(0,8)));//pego os 1° 8 digitos converto p decimal depois char
                //System.out.print("\nchar : "+c);
                reduzString(st,8);
               
                return new NoA(c, 0, null, null);
              
            }else if(st[0].equals("0")){
                
                reduzString(st,1);
                //System.out.print("\nchar : "+A);
                r = new NoA('\0', 0, leituraArvore(st), leituraArvore(st));//nós internos da arvor

            }
             
            return r;
    }

    public static  void reduzString(String[] st, int quant){//vai reduzindo a string

            int num = st.length;
            if(A.length() > 1){

                if(quant == 1){
                    A = A.substring(1, A.length());
                }else{
                    A = A.substring(8, A.length());
                }
    
                for (int i = 0; i < num; i++) {

                    if(i < A.length()){
                        st[i] = ""+A.charAt(i);
                    }else{
                        st[i] = "\0";
                    }  

                }

            }else{
                st[0] = "\0";
            }
           
    
    }

        
    public String escrever(){
        String escrita = "";
        System.out.print("\n\n----- Arvore Construção ----------\n");
        escrita = escreverArvore(raiz, escrita);
        System.out.print("\nArvore total : "+escrita+"\n");
        //System.out.print("\nQuantidade de Bits da arvore : "+escrita.length()+"\n");
        raiz = null;//-------------------------------------------------->>>>>Perigo!
        return escrita;
    }

    public void leitura(String tree){//gambi das gambi tentando fz um ponteiro 
            String[] novaTree = new String[TA];
            //System.out.print("\n-------Remontar Arvore------\n");
           
            A = tree.substring(0, TA);//------->>>corto excesso da arvore remontada, no caso do preenchimento de 8 bits
           // System.out.print("A: "+A); 
            for (int i = 0; i < novaTree.length; i++) {
                novaTree[i] = ""+tree.charAt(i);
            }

            raiz = leituraArvore(novaTree);//coloco a nova raiz 
    }

    public String decodificarArvore(String txtComprimido){//texto em bits
        int tamTxtOri = T, i = 0;
        NoA r = raiz;
        String texto = "",ch = txtComprimido;
        
        boolean folha = false;
        
        while(texto.length() < tamTxtOri){
            
            if(ch.charAt(i) == '0'){    
                r = r.getEsquerdo();

            }else {
                r = r.getDireito();
            }

            if((r.getEsquerdo() == null)&&(r.getDireito() == null)){
                folha = true;
                texto += r.getObjeto().getCaracter();
            }

            if(folha){
                folha = false;
                r = raiz;
            }
            i++;
        }

        return texto; 
    }



    public void decodificar(String textoCodigo){

        String textoDecodificado = decodificarArvore(textoCodigo);///
        
        
        try {
            escreverTxtDecodificado(textoDecodificado);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print("\nTexto decodificado:\n");
        System.out.print("\n"+textoDecodificado);
    }
   
    public static String converteDecimalParaBinario(int valor) {
        int resto = -1;
        String sb = "";
           
        if (valor == 0) {
            return "00000000";
        }
          
        // enquanto o resultado da divisão por 2 for maior que 0 adiciona o resto ao início da String de retorno
        while (valor > 0) {
            resto = valor % 2;
            valor = valor / 2;
            sb = resto+""+sb;
        }

        if(sb.length()< 8){

            while(sb.length() < 8){//adiciono zeros no inicio ate formar 1 bite completo
                sb = "0" + sb;
            }

        }
          
        return sb;
    }

    public static int converteBinarioParaDecimal(String valorBinario) {//binario para decimal
        int valor = 0;
          
            // soma ao valor final o dígito binário da posição * 2 elevado ao contador da posição (começa em 0)
            for (int i = valorBinario.length(); i > 0; i--) {
               valor += Integer.parseInt(valorBinario.charAt( i - 1)+"")*Math.pow(2, (valorBinario.length()-i));

            }
          
        return valor;
    }

    public void decodificacao() throws IOException{//leitura do arquivo comprimido
         decodificar(preLeitura());//com a arvore montada, o conteudo codificado é decodificado
        //System.out.println("tt: "+preLeitura());
    }

    
    private String preLeitura() throws IOException{

        InputStream is = null;
        DataInputStream dis = null;
        String posDecTxt = "",tam = "",tamArv ="", posDecArv ="";
        int passe = 0;
        
        try {

           is = new FileInputStream("textoComprimido.txt");
           dis = new DataInputStream(is);
    
           int count = is.available();
           byte[] bs = new byte[count];

           dis.read(bs);

           for (byte b:bs) {
           
              //conversao byte para decimal positivo, & 0xff torna o binario completo 2 em binario positivo
              int c = b & 0xff;//tipo -1 decimal é igual 11111111(inverte 00000000 e soma 1)
              //posDecTxt += converteDecimalParaBinario(c);

              if(passe == 0){//pego o tamanho da arvore

                if(c != '\n'){ 
                    tamArv += (char)c;

                  }else{
                    TA = Integer.parseInt(tamArv);
                    passe++;
                  }

              }else if(passe == 1){
                
                    if((posDecArv.length() >= TA)&&(c == '\n')){
                        //System.out.println("dddd "+posDecArv.length());///----
                        passe++;

                    }else{
                        posDecArv += converteDecimalParaBinario(c);
                    }
                    
              }else if(passe == 2){//pego o tamanho do texto

                if(c != '\n'){
                    tam += (char)c;

                  }else{
                    T = Integer.parseInt(tam);
                    passe++;

                  }

              }else{
                posDecTxt += converteDecimalParaBinario(c);
              }
 
           }
           
        } catch(Exception e) {
           
           e.printStackTrace();
        } finally {
           
           if(is!=null)
              is.close();

           if(dis!=null)
              dis.close();
        }

        
       // T = Integer.parseInt(tam);
        
          leitura(posDecArv);//remonta a arvore
       // System.out.println("\ntamanho texto : "+T);
        //System.out.println("\n\ntexto : "+posDecTxt);
        return posDecTxt;
    }

    private void escreverTxtDecodificado(String txt)throws IOException{//escrita do texto decodificado

        FileWriter fw = null;
        PrintWriter saida = null;

        File arq = new File("textoDescomprimido.txt");//criando arquivo de saida se nao existir

        if(arq.exists()){
            System.out.println("\n\n\nO arquivo textoDescomprimido.txt já existia");
        }else{
            System.out.println("\n\n\nO arquivo textoDescomprimido.txt nao existia");
            try {
                Path file = Paths.get("textoDescomprimido.txt");
                Files.createFile(file);
            } catch (IOException e) {
                System.out.println("Erro ao criar arquivo");
            }
        }
        
        String conteudo = txt;//variavel que armazenara o texto do arquivo

        try {
           fw = new FileWriter(arq);
           saida = new PrintWriter(fw);
           saida.println(conteudo);

        } catch (IOException e) {
            System.out.println("Erro ao escrever no arquivo");
        }finally{
            saida.close();
            fw.close();
        }

    }

}

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Scanner;

public class App {
     // Atributo para manipular o descritor e as operaÃ§Ãµes da lista.
    // Criando a instância da classe 'DescritorED'.
    static Descritor listaEncadeada;
    static Arvore arvore;
    public static void main(String[] args) throws IOException {

        // sc = Scanner
        Scanner sc = new Scanner(System.in);    
        int opcao;  // Opção para trabalhar o menu principal.  
        boolean ban = true;

        do{
            
            if(ban){
                System.out.println("\n\n");
                System.out.println(banner());
                ban = false;
            }
            
            System.out.println("\n\n");

            System.out.println("1 - Comprimir");
            System.out.println("2 - Descomprimir");
            System.out.println("0 - Sair");

            System.out.print("\nDigite a opção? ");
            opcao = sc.nextInt();
            

            switch(opcao){

                case 1: // comprimir 
                        listaEncadeada = new Descritor();//criação da lista
                        Leitura();//faz a leitura do arquivo entrada.txt.
                        
                        break;
                        
                case 2: // descomprimir
                        arvore = new Arvore();//para recriar a arvore
                        arvore.decodificacao();

                        break;
                
                default: System.out.println("\nSaindo da aplicação...");
                         
                         break;

            }

        }while(opcao != 0);

        sc.close();
        System.exit(0);
    }

    public static void Leitura() throws IOException {//leitura do arquivo entrada.txt

        FileReader fr = null;
        BufferedReader br = null;
        String conteudo ="";
        int tam = 0;

        try {

            fr = new FileReader("entrada.txt");//arquivo de entrada
            br = new BufferedReader(fr);

            String linha = br.readLine();
            char caracter = ' ';
           

            while (linha != null) {//enquanto tiver linhas no arquivo

                StringReader reader = new StringReader(linha);
                int k = 0;

                while ((k = reader.read()) != -1) { //leitura dos caracteres

                    caracter = (char) k;
                    conteudo += caracter;
                    tam++;
                }
                    

                linha = br.readLine();//pega a nova linha

                if(linha != null){
                    conteudo += '\n';//adiciona o proximo paragrafo
                    tam++;
                }
 
            }

        } catch (FileNotFoundException e) {
            System.out.println("Arquivo nao encontrado");

        } catch (IOException e) {
            System.out.println("Erro na leitura");

        } finally {

            
            fr.close();
            br.close();
            
        }
        

        inserir(conteudo);
        System.out.println(conteudo);
        System.out.print("\n\n-------Ordenacao------\n");
        listaEncadeada.ordenar();//faz a ordenação dos caracteres.
        System.out.println(listaEncadeada.relatorio());//mostra os caracteres e suas frequencias.
        System.out.print("\n\n-------Montar Arvore------\n\n");
        listaEncadeada.montarArvore();//monta a arvore segundo Huffman.
        listaEncadeada.compressao(conteudo,tam);//de acordo com a tabela obtida na arvore Huffman,
                                                                        // transforma o texto em codigo binario.

    }

    public static void inserir(String texto){

        int pos = -1;
        int posicao = 0;

        for (int i = 0; i < texto.length(); i++) {

            pos = listaEncadeada.pesquisarCaracter(texto.charAt(i));//pesquiso para ver se ja esta inserido o caracter

            if(pos != -1){
                listaEncadeada.alterar(pos);//se ja tiver inserido adiciono +1 a frequencia

            }else{
                listaEncadeada.inserir(1,texto.charAt(i),posicao,null);//inserção inicial do caracter
                posicao++;
            }
        }
        

    }

    public static String banner(){
        String ban = "                                                                                   \n"+
                     "  _|    _|                  _|_|       _|_|                                        \n"+ 
                     "  _|    _|   _|    _|     _|         _|       _|_|_|  _|_|       _|_|_|   _|_|_|   \n"+ 
                     "  _|_|_|_|   _|    _|   _|_|_|_|   _|_|_|_|   _|    _|    _|   _|    _|   _|    _| \n"+ 
                     "  _|    _|   _|    _|     _|         _|       _|    _|    _|   _|    _|   _|    _| \n"+ 
                     "  _|    _|     _|_|_|     _|         _|       _|    _|    _|     _|_|_|   _|    _| \n"; 
                                                                                          
                                                                                          
        return ban;
    }
    


}

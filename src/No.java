public class No {

    private NoA noA;
    private No antecessor;//antecessor
    private No sucessor;//sucessor
   

    public No(int frequencia, char caracter){        
        NoA n = new NoA(caracter, frequencia, null, null);
        noA = n;

    }

    public No(NoA n){
        noA = n;
    }

    public Usuario getObjeto() {
        return noA.getObjeto();
    }

    public void setObjeto(Usuario objeto) {
        noA.setObjeto(objeto);
    }

    public No getAntecessor() {
        return this.antecessor;
    }

    public void setAntecessor(No antecessor) {
        this.antecessor = antecessor;
    }
    
    public No getSucessor() {
        return this.sucessor;
    }

    public void setSucessor(No sucessor) {
        this.sucessor = sucessor;
    }
  
    public NoA getNoA() {
        return noA;
    }

    public void setNoA(NoA noA) {
        this.noA = noA;
    }

}

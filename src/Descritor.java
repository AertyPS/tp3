import java.io.*;

//imports ultilizados na leitura e escrita de arquivos 

public class Descritor {
      /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
      private No inicioDaLista;
      private int quantidadeDeNos;
      private No finalDaLista;
      private Arvore arvore = new Arvore();
      /*-----------------------------------
               CONSTRUTOR DA CLASSE
        -----------------------------------*/
  
      public Descritor(){
          inicializarLista();
      }
  
      /*-----------------------------------
              MÉTODOS get | set DA CLASSE
        -----------------------------------*/
  
      public int getQuantidadeDeNos() {
          return quantidadeDeNos;
      }
  
      /*-----------------------------------
                  OEPRAÇÕES DA LSE
        -----------------------------------*/
  
      // inicializar a lista
      private void inicializarLista(){
          inicioDaLista = null;
          quantidadeDeNos = 0;
          finalDaLista = null;
      }
  
      // Método para apagar a lista encadeada.
      public void apagarLista(){
          
          // Se a lista não estiver vazia, inicializa os valores!
          if ( !isEmpty() )
              inicializarLista();
          
      }
  
      // Método para verificar se a lista está vazia.
      public boolean isEmpty(){
          
          // Se a quantidade de nós da lista tiver o valor 0
          // quer dizer que a lista está vazia.
          return quantidadeDeNos == 0;
  
      }
  
      // Método para inserir um nó na lista
      public boolean inserir(int codigo, char caracter, int p, No noTrie){
          
          // Se posição negativa ou maior que a quantidade de nós
          if ( (p < 0) || (p > quantidadeDeNos) ){
              
              return false; // Quer dizer que não será possível inserir dados.
          
          }else{
  
              // Cria-se uma instância da classe 'No'.
              No novoNo;
              if(caracter !='\0'){
                novoNo = new No(codigo, caracter);
              }else{
                novoNo = noTrie;
              }
              
              // Caso 1: Lista está vazia.
              if ( isEmpty() ){
  
                  novoNo.setAntecessor(null);
                  novoNo.setSucessor(null);
  
                  inicioDaLista = novoNo;
                  finalDaLista = novoNo;
                  
              }else{
  
                  // Caso 2: posição = 0
                  if (p == 0){
  
                      inicioDaLista.setAntecessor(novoNo);
                      novoNo.setSucessor(inicioDaLista);
                      novoNo.setAntecessor(null);
                      inicioDaLista = novoNo;
                      
                  }else{
  
                       // Caso 3: posição = ultima posição válida
                      if (p == quantidadeDeNos){
  
  
                          finalDaLista.setSucessor(novoNo);
                          
                          novoNo.setAntecessor(finalDaLista);
  
                          // Registra null como próxima referência.
                          novoNo.setSucessor(null);
  
                          // O último nó da lista a partir deste momento
                          // tem o endereço do novoNo.
                          //finalDaLista.getReferencia().setReferencia(novoNo); <--- Fiz esse erro.
                          //finalDaLista.setReferencia(novoNo); // <--- Código correto.
  
                          // Registra o novoNo como último nó da lista.
                          finalDaLista = novoNo;
                          
                      }else{
                          
                              // Começa do final da lista.
                              No auxiliar = finalDaLista;
  
                              int i = quantidadeDeNos-1;
  
                                  //  4 > 4
                              while ( i > p ){
                                  
                                  auxiliar = auxiliar.getAntecessor();
  
                                  i--;
  
                              }
  
                              auxiliar.getAntecessor().setSucessor(novoNo);
                              novoNo.setAntecessor(auxiliar.getAntecessor());
                              novoNo.setSucessor(auxiliar);
                              auxiliar.setAntecessor(novoNo);
  
                          
                          
                      }
  
                  }
  
              }
  
              // Atualiza a quantidade de nós da lista.
              quantidadeDeNos = quantidadeDeNos + 1;
  
              return true;
  
          }
  
      }
  
      // Método para remover um nó da lista
      public boolean remover(int p){
  
          // Se a lista estiver vazia ou
          // Se posição negativa ou
          // Se posição maior ou igual a quantidade de nós
  
          if ( isEmpty() ||  (p < 0) || (p >= quantidadeDeNos)){
  
              return false; // Não é possível remover nós da lista.
  
          }else{
  
              // Caso 1: existe apenas 1 elemento.
              if (quantidadeDeNos == 1){
  
                  inicializarLista(); // Inicializa os valores do descritor da lista.
  
              }else{
  
                  // Atributo responsável por percorrer a lista encadeada.
                  // 'referenciaAtual' recebe uma cópia do endereço do nó
                  // que está na 1ª posição da lista.
                  No referenciaAtual = inicioDaLista;;
  
                  // Caso 2: posição == 0
                  if (p == 0){
                      
                      // Registro o início da lista como sendo o 2º nó da lista.
                      inicioDaLista = referenciaAtual.getSucessor();//modificado------><--------------------
  
                      // O nó a ser excluído, não terá mais acesso a outro nó.
                      referenciaAtual.setSucessor(null);//modificado
  
                  }else{

                    // Sentinela usado para percorrer até a posição desejada.
                    int i = 0;
                    
                    // Enquanto não chegar na posição anterior a que se
                    // quer remover o nó, vai percorrendo a lista.
                    while(i < (p - 1) ){

                        referenciaAtual = referenciaAtual.getSucessor();
                       // System.out.println("ultimo : "+referenciaAtual.getObjeto().getCaracter());
                        i++;
                    }

                    // Quando chegar na posição desejada, verificar se caso:
                    // 3 ou 4.
                   // System.out.println("q: "+quantidadeDeNos);

                    // Caso 3: remover o nó que está na última posição da lista.
                    if (p == (quantidadeDeNos - 1)){//----------------ultimo no

                        // Diz que o nó atual não terá mais acesso ao endereço
                        // do último nó da lista.
                        referenciaAtual.getSucessor().setAntecessor(null);
                        referenciaAtual.setSucessor(null);
                        finalDaLista.setSucessor(referenciaAtual);
                        
                         // O último nó da lista passa a ser
                        // o nó anterior ao que foi removido.
                        finalDaLista = referenciaAtual;

                    }else{

                        // Caso 4: remover quem está entre 0 e p.

                        // Copia o endereço do nó a ser removido.
                        No noAlvoReferencia = referenciaAtual.getSucessor();
                        No proxNoReferencia =  noAlvoReferencia.getSucessor();

                        // Registra o endereço do nó posterior ao nó a
                        // ser removido como endereço do nó atual (nó anterior
                        // ao que será removido.
                        referenciaAtual.setSucessor(noAlvoReferencia.getSucessor());//anterior recebe 
                        proxNoReferencia.setAntecessor(noAlvoReferencia.getAntecessor());
                        // O nó a ser removido não tem mais acesso ao 
                        // endereço do próximo nó.
                        noAlvoReferencia.setAntecessor(null);
                        noAlvoReferencia.setSucessor(null);

                    }

                }

  
              }
  
              // Atualiza a quantidade de nós da lista.
              quantidadeDeNos = quantidadeDeNos - 1;
  
              return true;
  
          }
  
      }

      public boolean alterar(int pos){

        int p = 0,f = 0;

        if ( !isEmpty() ){
              
            // Atributo para percorrer a lista encadeada.
            No referenciaAuxiliar = inicioDaLista;
            
            // Enquanto não chegar no final da lista.
            while ( referenciaAuxiliar != null ){
                
                if(pos == p){
                    f = referenciaAuxiliar.getObjeto().getFrequencia();//pego a frequencia 
                    f++;
                    referenciaAuxiliar.getObjeto().setFrequencia(f);//adiciono a nova frequencia
                    return true;//quebro o while
                }
              
                // Avança para  o próximo nó.
                referenciaAuxiliar = referenciaAuxiliar.getSucessor();
                p++;
            }
                      
        }

        return false;
      }

      public boolean ordenar(){//ordenação crescente

        int freq1 = 0;//frequencia a ser comparada
        int freq2 = 0;
        int auxF = 0, auxF2 = 0;
        char auxC = ' ', auxC2 =' ';
       

        if ( !isEmpty() ){
              
            // Atributo para percorrer a lista encadeada.
            No referenciaAuxiliar1 = inicioDaLista;
            No referenciaAuxiliar2 = inicioDaLista;
            
            // Enquanto não chegar no final da lista.
            while ( referenciaAuxiliar1 != null ){
                
                // Vai resgatando os valores do nó (dados dos caracters) e já vai
                freq1 = referenciaAuxiliar1.getObjeto().getFrequencia();
                
                while ( referenciaAuxiliar2 != null ){
                
                    freq2 = referenciaAuxiliar2.getObjeto().getFrequencia();
                   // System.out.println(" f1 "+freq1+"f2 "+freq2);
                    
                        if(freq1 > freq2){
                           // System.out.println("- troca -");//para nao haver erro
                            auxC = referenciaAuxiliar1.getObjeto().getCaracter();
                            auxF = referenciaAuxiliar1.getObjeto().getFrequencia();

                            auxC2 = referenciaAuxiliar2.getObjeto().getCaracter();
                            auxF2 = referenciaAuxiliar2.getObjeto().getFrequencia();

                            referenciaAuxiliar1.getObjeto().setCaracter(auxC2);
                            referenciaAuxiliar2.getObjeto().setCaracter(auxC);

                            referenciaAuxiliar1.getObjeto().setFrequencia(auxF2);
                            referenciaAuxiliar2.getObjeto().setFrequencia(auxF);
                            
                        }
                    
                    // Avança para  o próximo nó.
                    referenciaAuxiliar2 = referenciaAuxiliar2.getSucessor();

                    freq1 = referenciaAuxiliar1.getObjeto().getFrequencia();
                    
                }

                // Avança para  o próximo nó.
                referenciaAuxiliar1 = referenciaAuxiliar1.getSucessor();
                referenciaAuxiliar2 = referenciaAuxiliar1;
            }
                      
        }
        
        return true;
      }

      public int pesquisar(int f){// tem que esta ordenado, pesquisa pela frequencia
                                //retorna a ultima frequecia caso tenha freq. repetidas
        int f2 = 0;//variavel a ser comparada
        int p = 0;//posição
        int aux = -1;

        if ( !isEmpty() ){
              
            // Atributo para percorrer a lista encadeada.
            No referenciaAuxiliar = inicioDaLista;
            
            // Enquanto não chegar no final da lista.
            while ( referenciaAuxiliar != null ){
                
                // Vai resgatando os valores do nó (dados dos caracters) e já vai
                f2 = referenciaAuxiliar.getObjeto().getFrequencia();
                //System.out.println(f2);

                if(f2 <= f){
                    aux = p;
                }

                if(f2 > f){
                    return aux;
                }
                
                // Avança para  o próximo nó.
                referenciaAuxiliar = referenciaAuxiliar.getSucessor();
                p++;
            }

        }
        return aux;

      }

      public int pesquisarCaracter(char c){//retorna a posicao na lista

        //retorna a ultima frequecia caso tenha freq. repetidas
        char c2 = 0;//variavel a ser comparada
        int p = 0;//posição

        if ( !isEmpty() ){

        // Atributo para percorrer a lista encadeada.
        No referenciaAuxiliar = inicioDaLista;

        // Enquanto não chegar no final da lista.
                while ( referenciaAuxiliar != null ){

                    // Vai resgatando os valores do nó (dados dos caracters) e já vai
                    c2 = referenciaAuxiliar.getObjeto().getCaracter();
                    //System.out.println(f2);

                    if(c2 == c){
                        return p;
                    }

                    // Avança para  o próximo nó.
                    referenciaAuxiliar = referenciaAuxiliar.getSucessor();
                    p++;
                }

            }

            return -1;

        }

      // Método para imprimir a lista encadeada (relatório dos dados dos caracters)
      public String relatorio(){
          
          // Atributo responsável por armazenar os dados dos usuários.
          // presentes na lista.
          String relatorio = "";
          
          // Se a lista não estiver vazia, que dizer que existe pelo menos 1 nó na lista.
          if ( !isEmpty() ){
              
              // Atributo para percorrer a lista encadeada.
              No referenciaAuxiliar = inicioDaLista;
              
              // Enquanto não chegar no final da lista.
              while ( referenciaAuxiliar != null ){
                  
                  // Vai resgatando os valores do nó (dados dos caracters) e já vai
                  // formatando a string de retorno.
                  relatorio = relatorio + "\n[ " + referenciaAuxiliar.getObjeto().getCaracter() + " -> ";
                  relatorio = relatorio + referenciaAuxiliar.getObjeto().getFrequencia() + " ]";
                  
                  // Avança para  o próximo nó.
                  referenciaAuxiliar = referenciaAuxiliar.getSucessor();
                  
              }
                        
          }
         
          // Retorna a string vazia ou com dados.
          return relatorio;
          
      }

      public boolean montarArvore(){

        if ( !isEmpty() ){
                
            No referenciaAuxiliar = inicioDaLista;
            char caracter1 = ' ', caracter2 = ' ';
            int frequencia1 = -1, frequencia2 = -1;
            

            while ( referenciaAuxiliar != null ){

                
                caracter1 = referenciaAuxiliar.getObjeto().getCaracter();
                frequencia1 = referenciaAuxiliar.getObjeto().getFrequencia(); 
                NoA noA1Esq = new NoA();
                NoA noA1Dir = new NoA();

                if(caracter1 == '\0'){
                        noA1Dir = referenciaAuxiliar.getNoA().getDireito();
                        noA1Esq = referenciaAuxiliar.getNoA().getEsquerdo();
                }else{
                    noA1Dir = null;
                    noA1Esq = null;
                }
                
                System.out.println(" f: "+frequencia1+" c: "+ caracter1);

                remover(0);
                referenciaAuxiliar = inicioDaLista;


                if( referenciaAuxiliar != null ){
                    caracter2 = referenciaAuxiliar.getObjeto().getCaracter();
                    frequencia2 = referenciaAuxiliar.getObjeto().getFrequencia(); 
                    NoA noA2Esq = new NoA();
                    NoA noA2Dir = new NoA();

                    if(caracter2 == '\0'){
                            noA2Dir = referenciaAuxiliar.getNoA().getDireito();
                            noA2Esq = referenciaAuxiliar.getNoA().getEsquerdo();
                    }else{
                        noA2Dir = null;
                        noA2Esq = null;
                    }
                    

                    NoA noA1 = new NoA(caracter1, frequencia1, noA1Esq, noA1Dir);
                    NoA noA2 = new NoA(caracter2, frequencia2, noA2Esq, noA2Dir);
                    
                    NoA noA3 = arvore.montar((frequencia1 + frequencia2), '\0', noA2, noA1);
                   
                    System.out.println(" f: "+frequencia2+" c: "+caracter2);

                    No no = new No(noA3);

                    remover(0);
                    inserir(-1, '\0', (pesquisar(frequencia1 + frequencia2)+1), no);//reincersao do no na lista
                    referenciaAuxiliar = inicioDaLista;

                }else{//se tiver so um no

                    NoA noA4 = new NoA(caracter1, frequencia1, null, null);
                   
                    arvore.montar(frequencia1, '\0', noA4, null);
                }
            
            }
                   
        }
        
        
        return true;
    }

    public void compressao(String texto, int tam)throws IOException{
       
        String binaria = "";
        String[] st = arvore.ConstruirTabela();
        String compressaoArvore = arvore.escrever();
       
            for (int i = 0; i < texto.length(); i++) {//pegando os dados para escrita no arquivo
                binaria += st[texto.charAt(i)];//trocando letras por codigo
            }
            
            System.out.println("\n------Texto Codificado-------");
            
            int tamArv = compressaoArvore.length();
            int tamTexto = (texto.length())*8;
            int tamBin = binaria.length();
            System.out.println("\nQuantidade de Bits do texto Original: "+tamTexto);
            System.out.println("Quantidade de Bits do texto Codificado: "+tamBin);
            System.out.println("Quantidade de Bits da Arvore : "+tamArv);
            double res = (((tamTexto - tamBin)*100)/tamTexto);
            System.out.println("\nSem a arvore Redução: "+res+"%");
            res = (((tamTexto - (tamBin + tamArv))*100)/tamTexto);
            System.out.println("Com a arvore Redução: "+res+"%");
    
            escritaCompressiva(binaria, tam, compressaoArvore, tamArv);
       
            
    }

    public void escritaCompressiva(String txt, int tamanho, String txtArvore, int tamArvore) throws IOException{
        
        String texto = txt;
        DataOutputStream dataOutputStream =
                new DataOutputStream(
                        new FileOutputStream("textoComprimido.txt"));

        int b;
        String arvTam = "";
        arvTam =""+tamArvore;
       // System.out.println("Caracteres da arvore Original :"+arvTam);
        
        for (int j = 0; j < arvTam.length(); j++) { //adiciona o tamanho da arvore
            String binario = Arvore.converteDecimalParaBinario(arvTam.charAt(j));
           
            b = (char) (Integer.parseInt (binario,2));
            dataOutputStream.writeByte(b); 
        }

        dataOutputStream.writeByte('\n');

        while (txtArvore.length() > 0) {//adiciono a arvore
        
            while (txtArvore.length() % 8 != 0)
                txtArvore += "0";// adiciona 0 ao final se nao tiver 8 bits      

                String binario = txtArvore.substring(0, 8); 
                b = (char) (Integer.parseInt (binario,2));//pulo do gato
                txtArvore = txtArvore.substring(8, txtArvore.length());
                dataOutputStream.writeByte(b);
        }

        dataOutputStream.writeByte('\n');

        String textoTam = "";
        textoTam =""+tamanho;
        //System.out.println("Caracteres do texto Original :"+textoTam);
        
        for (int i = 0; i < textoTam.length(); i++) { //adiciona o tamanho do texto
            String binario = Arvore.converteDecimalParaBinario(textoTam.charAt(i));
           
            b = (char) (Integer.parseInt (binario,2));
            dataOutputStream.writeByte(b);
        }

        dataOutputStream.writeByte('\n');

        while (texto.length() > 0) {         //adiciona o texto
            while (texto.length() % 8 != 0)
                texto += "0";// adiciona 0 ao final se nao tiver 8 bits      

                String binario = texto.substring(0, 8); //pelo os primeiros oito bits
                b = (char) (Integer.parseInt (binario,2));//pulo del gato
                texto = texto.substring(8, texto.length());//corto a string
                dataOutputStream.writeByte(b);//escrevo no arquivo
        }
         
        dataOutputStream.close();
    }

}

public class Usuario{

    /*-----------------------------------
              ATRIBUTOS DA CLASSE
      -----------------------------------*/
    
    private int frequencia;
    private char caracter;
    
    /*-----------------------------------
             CONSTRUTORES DA CLASSE
      -----------------------------------*/

    public Usuario(int frequencia, char caracter){
        this.frequencia = frequencia;
        this.caracter = caracter;
    }

    /*-----------------------------------
            MÉTODOS get | set DA CLASSE
      -----------------------------------*/

    public int getFrequencia() {
        return this.frequencia;
    }

    public void setCaracter(char c) {
        this.caracter = c;
    }

    public void setFrequencia(int f) {
        this.frequencia = f;
    }

    public char getCaracter() {
        return this.caracter;
    }

    public String toString(){

        return " [ Código: " + this.frequencia + " | " + "Nome: " +  this.caracter + " ]\n";

    }
    
}

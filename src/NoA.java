public class NoA {//no que sera uma arvore

    private Usuario objeto;
    private NoA direito;//direita
    private NoA esquerdo;//esquerda 
    
    public NoA(){}

    public NoA(char caracter, int frequencia, NoA esquerdo, NoA direito){

        Usuario u = new Usuario(frequencia,caracter);
        objeto = u;

        this.esquerdo = esquerdo;
        this.direito = direito;
    }

    public Usuario getObjeto() {
        return objeto;
    }

    public void setObjeto(Usuario objeto) {
        this.objeto = objeto;
    }

    public NoA getEsquerdo() {
        return esquerdo;
    }

  
    public void setEsquerdo(NoA esquerdo) {
        this.esquerdo = esquerdo;
    }


    public NoA getDireito() {
        return direito;
    }

    public void setDireito(NoA direito) {
        this.direito = direito;
    }
}
